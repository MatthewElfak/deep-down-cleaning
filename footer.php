

<div class="container-fluid carers" id="career-section">
	<div class="row" data-aos="fade-right">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 style="color: rgb(237,41,123); font-weight: bold !important;">CAREERS</h2>
					<p>At Deep Down Cleaning Services Ltd. we only hire the best. If you want to join our team, please fill in the
					form below or send your resume along with a cover letter by fax.</p>

					<p>To work for Deep Down Cleaning Services Ltd. you will need to demonstrate flexibility, honesty, reliability,
					integrity and a hard-working mind-set.</p>

					<p>You will need to supply a copy of your criminal record check and you must be bondable. A reliable method of transportation is an asset.</p>

					<p>References are a must.</p>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid application-form">
	<div class="row" data-aos="flip-left">
		<div class="container">
			<div class="row" style="padding: 25px;">
				<div class="col-12 col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center app-form">
					<h2>Application form</h2>
					<p style="cursor: default;"></p>

					<div class="row">
						<div class="col-10 offset-1 text-center" id="msgg" style="background-color: green; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;">
							<h6 style="margin-bottom: 0px;">Message sent successfully!</h6>
						</div>
						<div class="col-10 offset-1 text-center" id="msggg" style="background-color: red; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;"">
							<h6 style="margin-bottom: 0px;">Message could not be sent.</h6>
						</div>
						<div class="col-10 offset-1 text-center" id="req" style="background-color: red; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;"">
							<h6 style="margin-bottom: 0px;">All field is required!!!</h6>
						</div>
					</div>

					<form id="applicationForm">
					<div class="row">
						
						<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
							<input id="name" class="form-control" type="text" name="fname" placeholder="Full Name" required="" value="">
						</div>
						<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
							<input id="email" class="form-control" type="email" name="email" placeholder="Email" required="">
						</div>
						<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
							<input id="number" class="form-control" type="number" name="number" placeholder="Phone Number" required="" type="number">
						</div>
						<div class="form-group col-12">
							<select id="services" class="form-control" name="services">
								<option selected="true" disabled="" value="">What position are you applying for ?</option>
								<option value="Evening & night office cleaner">Evening & night office cleaner</option>
								<option value="Daytime apartment cleaning">Daytime apartment cleaner</option>
								<option value="Daytime building cleaner">Daytime building cleaner</option>
								<option value="Carpet cleaning technician">Carpet cleaning technician</option>
								<option value="Weekend staff">Weekend staff</option>
								<option value="Supervisor">Supervisor</option>
								<option value="Pressure washing specialist">Pressure washing specialist</option>
							</select>
						</div>
						<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
							<label for="resume" class="form-control btn btn-default">Upload Your Resume</label>

							<input style="display: none;" id="resume" type="file" name="files[]">
						</div>
						<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
							<label for="letter" class="form-control btn btn-default">Upload Your Cover Letter</label>

							<input style="display: none;" id="letter" type="file" name="files[]">
						</div>
						<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
							<div id="send" style="cursor: pointer; background-color: #ea297a; color: #fff; border-radius: 30px; font-weight: bold; transition: .125 all;" class="form-control btn btn-default">SEND</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<script>



	$("div#send").on('click', function(event){
		//disable the default form submission
	  

	
	  var name = $('#name').val();
	  var email = $('#email').val();
	  var number = $('#number').val();
	  var resume = $('#resume').val();
	  var letter = $('#letter').val();
	  var services = $('#services').val();

	  

	  if(name == "" || email == "" || number == "" || resume == "" || services =="" || letter == ""){

		setTimeout(function(){$('#req').css('display','block'); }, 100);
		setTimeout(function(){$('#req').css('display','none'); }, 2500);

	  }else{

	  	event.preventDefault();

		var form = $(this).closest('form');
		console.log(form);
		 
		  //grab all form data  
		var formData = new FormData($(this).closest('form')[0]);

		console.log(formData);


	  	 $.ajax({
	    url: 'send2.php',
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (returndata) {
	      setTimeout(function(){$('#msgg').css('display','block'); }, 1000);
	      	$('#msggg').css('display','none');
	    }
	  });

	 $("#applicationForm")[0].reset();
	 
	  return false;
		
	  }



	 });

	  
</script>




<div class="container-fluid contact-form" id="contact-section">
	<div class="row" data-aos="zoom-in-up">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 text-center con-form">
					<h2 style="font-weight: bold; color: #ea297a;">CONTACT US</h2>
					<p style="cursor: default;"></p>
						
					<div class="row">
						<div class="col-12 col-sm-12 col-md-8 col-lg-8 form-field">

						<div class="row">
							<div class="col-10 offset-1 text-center" id="msgg1" style="background-color: green; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;">
								<h6 style="margin-bottom: 0px;">Message sent successfully!</h6>
							</div>
							<div class="col-10 offset-1 text-center" id="msggg1" style="background-color: red; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;"">
								<h6 style="margin-bottom: 0px;">Message could not be sent.</h6>
							</div>
							<div class="col-10 offset-1 text-center" id="reqq" style="background-color: red; color: #fff !important; display: none; padding: 10px 10px; margin-bottom: 20px; border-radius: 10px;"">
							<h6 style="margin-bottom: 0px;">All field is required!!!</h6>
						</div>
						</div>

							<form id="contactForm">
							<div class="row">
								<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
									<input class="form-control" id="fname" type="text" name="fname" placeholder="Full Name" required="">
								</div>
								<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
									<input class="form-control" id="eemail" type="email" name="eemail" placeholder="Email" required="">
								</div>
								<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
									<input class="form-control" id="nnumber" type="number" name="nnumber" placeholder="Phone Number" required="">
								</div>
								<div class="form-group col-12">
									<select class="form-control" id="services" name="services">
										<option selected="true" disabled="" value="">Subject</option>
										<option value="Carpet & Upholstery Cleaning">Carpet & Upholstery Cleaning</option>
										<option value="Parkade Machine Power Sweeping & Scrubbing">Parkade Machine Power Sweeping & Scrubbing</option>
										<option value="Outdoor Machine Power Sweeping">Outdoor Machine Power Sweeping</option>
										<option value="Parking Lot Cleaning & Management">Parking Lot Cleaning & Management</option>
										<option value="Stripping & Waxing Floors">Stripping & Waxing Floors</option>
										<option value="Commercial Office Cleaning">Commercial Office Cleaning</option>
										<option value="Common Area Building Cleaning">Common Area Building Cleaning</option>
										<option value="24 Hour Emergency Service">24 Hour Emergency Service</option>
										<option value="Post-Construction Clean Up">Post-Construction Clean Up</option>
										<option value="Pressure Washing">Pressure Washing</option>
										<option value="Floods-Water Restoration">Floods-Water Restoration</option>
										<option value="Feedback">Feedback</option>
										<option value="Other">Other</option>
									</select>
								</div>
								<div class="form-group col-12 gorila-section">
									<textarea class="form-control" id="message" name="message" cols="30" rows="15" placeholder="Message"></textarea>
									<img class="img-fluid" src="images/gorila.jpg">
								</div>
								<div class="form-group col-12 sol-sm-12 col-md-4 offset-md-8">
									<div class="form-control btn-default" id="send2">SEND</div>
								</div>
							</div>
							</form>
						</div>

						<div class="col-12 col-sm-12 col-md-4 col-lg-4 text-sm-center text-md-left text-lg-left form-text">
							<img src="images/logo-contact.png" alt="Deep Down Cleaning Ltd">
							<p><span class="blue-color">Address:</span> Deep Down Cleaning Services Ltd. </p>
							<p>70 Crane Lake Drive Halifax, NS B3S 1B4.</p>
							<br>
							<p><span class="blue-color">Office: </span><a style="color: #111e6d; text-decoration: none;" href="tel:9024255619">(902) 425-5619</a></p>
							<p><span class="blue-color">After hours or emergency: </span><a style="color: #111e6d; text-decoration: none;" href="tel:9028301113">(902) 830-1113</a></p>
							<p><span class="blue-color">Fax: </span> (902) 444-3224</p>
							<br>
							<p><span class="blue-color">Email: </span> FastResponse@DeepDownCleaning.ca</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$("div#send2").on('click', function(event){


	  var fname = $('#fname').val();
	  var eemail = $('#eemail').val();
	  var nnumber = $('#nnumber').val();
	  var sservices = $('#services option:selected').text();
	  var message = $('#message').val();

	  if(fname == "" || eemail == "" || nnumber == "" || sservices == "" || message ==""){

		setTimeout(function(){$('#reqq').css('display','block'); }, 100);
		setTimeout(function(){$('#reqq').css('display','none'); }, 2500);

	  }else{

	  		//disable the default form submission
			  event.preventDefault();

			  var form = $(this).closest('form');
			    console.log(form);
			 
			  //grab all form data  
			  var formData = new FormData($(this).closest('form')[0]);

			  console.log(formData);
	  	
	  	 $.ajax({
		    url: 'send.php',
		    type: 'POST',
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (returndata) {
		      setTimeout(function(){$('#msgg1').css('display','block'); }, 1000);
		      	$('#msggg1').css('display','none');
		    }
		  });

		  $('#contactForm')[0].reset();
		 
		  return false;

	  }


	 
	 
	});

</script>


<div class="container-fluid who-we-are" id="who-section">
	<div class="row" data-aos="fade-right">
		<div class="container">
			<div class="row who-introduction">
				<div class="col-12 text-center">
					<h2 style="font-weight: bold !important; color: #ea297a;">WHO WE ARE</h2>
					<p>Deep Down Cleaning Services Ltd. was established in 1996. We provide fast, reliable cleaning services to commercial customers based in Halifax, Dartmouth, Bedford and Sackville. We are bonded and insured, with $5 million liability.</p>
					<p>We provide extremely fast and reliable service to all our customers: this is a reputation we have earned, one we're proud of, and one that makes us unique when compared to our competitors. We are committed to providing our clients with superior workmanship and 24/7 service. What we consistently hear from our clients is that we always say “Yes”. We know every part of the business, because our managers/owners maintain hands-on involvement with all accounts. With Deep Down Cleaning Services you can always expect - and be assured of - the highest quality of service response and professionalism.</p>
					<p>We are committed to providing our clients with superior workmanship and 24/7 service. What we consistently hear from our clients is that we always say “Yes”.</p>
				</div>
			</div>
			<div class="row" data-aos="fade-left">
				<div class="col-12 text-center certificate">
					<h2 style="font-weight: bold; color: #ea297a;">CERTIFICATE</h2>
					<p>We are committed to providing exceptional quality of service to our customers. Deep Down Cleaning Services Ltd. meets the highest standards of health, safety and quality.
					Deep Down Cleaning has been awarded the Contractor Check Accreditation, is COR recognized by the Construction Safety of Nova Scotia, and IICRC certified.</p>
					<h1>
						<span><img src="images/contractor.jpg" alt="constructor"></span>
						<span><img src="images/crc.jpg" alt="crc"></span>
						<span><img src="images/certificates/cor.png" alt="cor"></span>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid subscribe">
	<div class="row" data-aos="flip-down">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h3 style="color: #111e6c !important;">Join our mailing list to be the first to know about <br> our updates and promotions!</h3>
				</div>
			</div>
			<div class="row subscribe-form">
				<div class="col-12 col-sm-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2">
					<form action="#" method="POST">	
						<div class="row">
							<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
								<input class="form-control" type="text" name="name" placeholder="Name" required="">
							</div>
							<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">	<input class="form-control" type="email" name="email" placeholder="Email" required="">
							</div>
							<div class="form-group col-12 col-sm-12 col-md-4 col-lg-4">
								<button style="cursor: pointer;" class="btn btn-default form-control">SIGN UP</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid footer-wrap">
	<div class="row">
				<div class="col-12 text-center">
					<i class="fab fa-facebook-f" style="border-radius: 60%;
    border: 1px solid white;
    /* padding: 10px; */
    padding-left: 16px;
    padding-right: 16px;
    padding-top: 10px;
    padding-bottom: 10px;"></i>
					<i class="far fa-envelope" onclick="scrollIt('contact-section')" style="border-radius: 60%;
    border: 1px solid white;
    padding: 10px 11px;"></i>
				</div>
				<div class="col-12 text-center">
					<p style="font-size: 1rem; font-weight: 300;">For fast reliable service call: (902) 425-5619</p>
				</div>
				<div class="col-12">
					<ul class="list-inline text-center">
						<li onclick="scrollIt('who-section')" class="list-inline-item text-md-center text-lg-center">ABOUT US</li>
							<a onclick="scrollIt('services-section')"><li class="list-inline-item text-md-left text-lg-center">SERVICES</li></a>
							<a onclick="scrollIt('testimonials-section')"><li class="list-inline-item">TESTIMONIALS</li></a>
							<a class="text-center" onclick="scrollIt('news-section')"><li class="list-inline-item">NEWS</li></a>
							<a onclick="scrollIt('specials-section')"><li class="list-inline-item">SPECIALS</li></a>
							<a onclick="scrollIt('career-section')"><li class="list-inline-item">CAREERS</li></a>
							<a onclick="scrollIt('contact-section')"><li class="list-inline-item">CONTACT US</li></a>
							<li class="list-inline-item" data-toggle="modal" data-target="#exampleModal">PRIVACY POLICY</li>
							<li class="list-inline-item">SITEMAP</li>
					</ul>
				</div>
	</div>
</div>
</div></div></div></div></div>



<div class="container-fluid psmedia">
	<div class="row">
		<div class="col-12 text-center">
			<p>Designed, hosted and powered by <a target="_blank" href="http://www.phaseshiftmedia.com">phaseshiftmedia.com</a></p>
		</div>
	</div>
</div>




<div style="z-index: 10000 !important;" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Privacy Policy</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>We are committed to protecting the privacy of the personal information of our clients, employees, partners and other stakeholders. Your information will never be passed on or sold to any other person or company.</p>
        <p>If you have any comments or questions about this statement, Deep Down Cleaning Services Ltd., or the practices of this site, you can contact us through our Contact Us page, or call us directly at (902) 425-5619.</p>
      </div>
    </div>
  </div>
</div>