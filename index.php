<!DOCTYPE html>
<html>
<head>
	<title>Deep Down Cleaning | Home</title>

	<!-- Meta Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap Includes -->

	<link rel="stylesheet" type="text/css" href="library/bootstrap/css/bootstrap.min.css">
	<script type="library/bootstrap/js/bootstrap.min.js"></script>

	<!-- Custom Includes -->

	<link rel="stylesheet" type="text/css" href="assets/css/index.css">
	<link rel="stylesheet" type="text/css" href="assets/css/header.css">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>
<body>

	<?php require_once "header.php"; ?>

	<div class="container-fluid landing-container" style="position: relative;">
		<div class="row">
			<div class="col-12 text-center mobile-gor">
				<img class="img-fluid align-self-start" src="images/gor.png">
			</div>
			<div class="col-12" style="position: absolute; bottom: 0;">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 front-text">

							<div class="row h-100">
								<div class="col-12 col-sm-12 col-md-7 col-lg-6 text-center text-md-left align-self-center">
									<h5>Need professional<br>fast reliable service?</h5>
								</div>
								<div class="col-12 col-sm-12 col-md-5 col-lg-6 text-center text-md-right align-self-center">
									<button class="btn btn-default" onclick="scrollIt('contact-section')"><a style="text-decoration: none; color: #fff;" href="tel:9024255619">CALL US NOW</a></button>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div style="position: absolute; bottom: 0; right: 0;" class="col-12 col-sm-12 col-md-4 col-lg-4 text-right">
							<img style="float: right;" class="img-fluid gor" src="images/gor.png">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container certificate-image">
		<div class="row">
			<div class="col-4 text-center  align-self-center">
				<img class="img-fluid" src="images/certificates/check.png" alt="">
			</div>
			<div class="col-4 text-center align-self-center">
				<img class="img-fluid" src="images/certificates/irc.png" alt="">
			</div>
			<div class="col-4 text-center">
				<img class="img-fluid" src="images/certificates/cor.png" alt="">
			</div>
		</div>
	</div>



	<div class="container-fluid services-container-fluid" id="services-section" style="padding-left: 0px; padding-right: 0px;">

		<div class="row">
			<div class="col-12 bottom-top-margin">
				<h2 class="text-center" style="font-weight: bold !important; color: #ea297a;">SERVICES</h2>
			</div>
		</div>

		<div class="bubble"><img src="images/water-bubble.png"></div>

		<div class="bubble1"><img src="images/water-bubble.png"></div>

		<div class="row">

			<div class="bubble2"><img src="images/water-bubble.png"></div>

			<div class="bubble3"><img src="images/water-bubble.png"></div>

			<div class="bubble33"><img src="images/water-bubble.png"></div>
<div class="container">
	<div class="row" data-aos="fade-right">
			<div class="col-md-4 servisi" style="border-right: 1px solid rgb(17,28,107);">
				<div class="col">
					<p class="text-center long"><span class="selected sections">Residential/Commercial Carpet & Upholstery Cleaning</span></p>
				</div>
				<div class="col">
					<p class="text-center long"><span class="sections">Parkade Machine Power Sweeping & Scrubbing</span></p>
				</div>
				<div class="col">
					<p class="text-center super-long"><span class="sections">Floods – Water Restoration (IICRC certified)</span></p>
				</div>
			</div>
			<div class="col-md-4 servisi srv2" style="border-right: 1px solid rgb(17,28,107); padding-top: 10px;">
				<div class="col">
					<p class="text-center"><span class="sections">Pressure Washing</span></p>
				</div>
				<div class="col">
					<p class="text-center"><span class="sections">Outdoor Machine Power Sweeping</span></p>
				</div>
				<div class="col">
					<p class="text-center long"><span class="sections">Parking Lot Cleaning & Management</span></p>
				</div>
				<div class="col">
					<p class="text-center"><span class="sections">Stripping & Waxing Floors</span></p>
				</div>
			</div>
			<div class="col-md-4 servisi srv2" style="padding-top: 10px; border-right: 1px solid white;">
				<div class="col">
					<p class="text-center"><span class="sections">Commercial Office Cleaning</span></p>
				</div>
				<div class="col">
					<p class="text-center"><span class="sections">Common Area Building Cleaning</span></p>
				</div>
				<div class="col">
					<p class="text-center"><span class="sections">24 Hour Emergency Service</span></p>
				</div>
				<div class="col">
					<p class="text-center"><span class="sections">Post-Construction Clean Up</span></p>
				</div>
			</div>
		</div>
	</div>
</div>

	<script>
	window.onbeforeunload = function () {
	  window.scrollTo(0, 0);
	}
	</script>
	

	<script>
		var array = document.getElementsByClassName('sections');
		$( "span.sections" ).click(function() {
		  if($(window).width()<768){
		  	$('html, body').animate({
	    	scrollTop: $("#change-container").offset().top - 140
			}, 1400);
		  }
		  else if($(window).width()<=768 && $(window).width()>=576){
		  	$('html, body').animate({
	    	scrollTop: $("#change-container").offset().top - 0
			}, 1400);
		  }
		  else if($(window).width()>768){
		  	$('html, body').animate({
	    	scrollTop: $("#change-container").offset().top - 110
			}, 1400);
		  }
		});
	</script>

		<div class="container services-container" id="change-container">
			<div class="row">
				<div class="col-12">
					<div class="col-12" style="display: inline-grid;">
						<h5 class="text-center align-self-center w-100" id="changing-service-name" style="position:absolute; color:rgb(17,28,107); z-index: 1001;">Residential/Commercial Carpet & Upholstery Cleaning</h5>
						<img id="changing-image" src="images/residential.jpg" class="img-fluid bottom-top-margin text-center" style="z-index: 1000; opacity: 0.55;" width="100%;">
					</div>
					<div class="col-12 text-center">
						<a class="telephone" href="tel:9024255619"><button id="first-btn" class="btn btn-default text-center margin-top-larger"  onclick="scrollIt('contact-section')" style="margin-bottom:30px; background-color:rgb(237,41,123);">CALL US NOW</button></a>
					</div>

					<!-- Upholstery Block -->
					<div class="col-12 blocks" id="upholstery" style="display: block;">

						<p class="text-center">Deep Down Cleaning Services Ltd. employs experienced carpet and upholstery cleaning professionals. The products we use are proven to be the best at removing stains, yet they retain your carpet’s original colour, texture and vibrancy.</p>

						<p class="text-center">At Deep Down Cleaning Services Ltd. we use the best carpet cleaning technology to give your carpets their deepest clean. Whether we use our truck-mounted system (for buildings up to 3 stories), or our powerful portable extractors, we can meet all your carpet cleaning needs. Deodorization is always free!</p>

						<p class="text-center">Our professional staff can bring your carpets back to life!</p>

	                    <!--<p class="text-center margin-top"><strong>FREE ESTIMATES</strong></p>-->
					</div>

					<!-- Parkade Block -->
					<div class="col-12 blocks" id="parkade" style="display: none;">

						<div class="col-12 text-center bottom-top-margin" style="font-weight: normal !important; margin-top: -5px; color:rgb(17,28,107); "><h3>COMPARE OUR SERVICES!</h3></div>

						<p class="text-center">Deep Down Cleaning's powerful machine power sweeper & scrubber will get your underground parkades and indoor concrete surfaces sparkling clean! Unlike our pressure-washing competitors, our system extracts the water so you never need to worry about clogged drains again!</p>

						<div class="container bottom-top-margin">


							<div class="row" style="margin-bottom: 50px; border: 2px solid #111e6d; border-radius: 30px;">
								<div class="col-12" style="overflow-y:auto">
									<table class="table" style="color: #111e6d !important;">
									  	<thead> 
										    <tr style="border:none;" class="h-100">
										      	<th style="border:none;" scope="col" class="align-middle"></th>
										      	<th style="border:none;" scope="co" class="text-center align-middle no-left">Machine Power Sweeping & Scrubbing</th>
										      	<th style="border:none;" scope="col" class="text-center align-middle no-left">Machine Power Sweeping Only</th>
										    </tr>
									  	</thead>
									  	<tbody>
										    <tr class="h-100">
										      	<td scope="row" class="align-middle">Dry sweep</td>
										      	<td class="text-center align-middle"><i class="fas fa-check"></i></td>
										      	<td class="text-center align-middle">
										      		<i class="fas fa-check"></i><br>
										      		A hand crew is provided to
													sweep out corners, edges
													and around poles
										      	</td>
										    </tr>
										    <tr class="h-100">
										    	<td scope="row" class="align-middle right-bo">Wet scrub of parkade with
												stain treatment and powerful
												water extraction leaving
												surfaces sparkling clean</td>
										    	<td class="text-center align-middle right-bo"><i class="fas fa-check"></i></td>
										    	<td class="text-center align-middle"><i class="fas fa-times"></i></td>
										    </tr>

										    <tr class="h-100">
										    	<td scope="row" class="align-middle right-bo">Removes surface dirt, dust
												and debris</td>
										    	<td class="text-center align-middle right-bo"><i class="fas fa-check"></i></td>
										    	<td class="text-center align-middle"><i class="fas fa-check"></i></td>
										    </tr>

										    <tr class="h-100">
										    	<td scope="row" class="align-middle right-bo">Oil and grease stains are
												treated and scrubbed away</td>
										    	<td class="text-center align-middle right-bo"><i class="fas fa-check"></i></td>
										    	<td class="text-center align-middle"><i class="fas fa-times"></i></td>
										    </tr>

										    <tr class="h-100">
										    	<td scope="row" class="align-middle right-bo">Can be done for indoor
												parkades, warehouses and
												concrete surfaces</td>
										    	<td class="text-center align-middle right-bo"><i class="fas fa-check"></i></td>
										    	<td class="text-center align-middle"><i class="fas fa-check"></i></td>
										    </tr>

										    <tr class="h-100">
										    	<td scope="row" class="align-middle right-bo">Can be done for outdoor
												parking lots, parkades
												warehouses and concrete
												surfaces</td>
										    	<td class="text-center align-middle right-bo">
										    		<i class="fas fa-times"></i><br>
													Scrubber cannot be used on
													outdoor surfaces
										    	</td>
										    	<td class="text-center align-middle"><i class="fas fa-check"></i></td>
										    </tr>
										    <tr class="h-100 last">
										    	<td scope="row" class="align-middle right-bo">A light deodorization is
												provided</td>
										    	<td class="text-center align-middle right-bo"><i class="fas fa-check"></i></td>
										    	<td class="text-center align-middle"><i class="fas fa-times"></i></td>
										    </tr>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<!-- Outdoor Block -->
					<div class="col-12 blocks" id="outdoor" style="display: none;">

						<p class="text-center">Deep Down Cleaning's power sweeper will ensure any asphalt or concrete surface is clean and dust-free.</p>

						<p class="text-center">First impressions count, so it is vital to keep your outdoor parking areas looking their best.</p>

						<p class="text-center">After a long, dirty winter have your outdoor areas machine swept by the professionals. Your property will look better than the rest.</p>

						<!--<p class="text-center bottom-top-margin"><strong>Call for your free estimate</strong></p>-->

					</div>


					<!-- Parking Block -->
					<div class="col-12 blocks" id="parking" style="display: none;">

						<p class="text-center">Regular cleaning and sweeping is one of the most important components of a successful business. Studies have clearly illustrated that cleanliness ranks as a top attribute in determining where people shop. So if the parking lot area is not clean, shoppers are deterred and may shop elsewhere.</p>

						<p class="text-center">We can schedule visits to your establishment Sunday to Friday, before 10 am, to remove any debris, garbage and spot sweep your property. This ensures a clean and professional environment for all tenants and visitors to your property.</p>

						<p class="text-center">Deep Down Cleaning Services Ltd. has been cleaning parking lots for over eight years and we have maintained all our contracts.</p>

						<p class="text-center">We want to work with you to save you money and give you peace of mind.</p>

						<!--<p class="text-center bottom-top-margin"><strong>Call for your free estimate</strong></p>-->

					</div>

					<!-- Stripping Block -->
					<div class="col-12 blocks" id="stripping" style="display: none;">

						<p class="text-center">Floors can lose their shine and get wax build up so it is important to maintain them on a regular basis.</p>

						<p class="text-center">Deep Down Cleaning Services Ltd. specialize in floor care services such as stripping, waxing, buffing, polishing, scrubbing and application of floor sealers.</p>

						<p class="text-center">Experts advise that regular floor maintenance will keep floor surfaces looking their best and increase their longevity.</p>

						<p class="text-center">Deep Down Cleaning Services Ltd. staff is well trained with today's state of the art equipment and environmentally-safe supplies and materials. </p>

						<!--<p class="text-center bottom-top-margin"><strong>Call for your free estimate</strong></p>-->

					</div>
  
					<!-- Commericial Block -->
					<div class="col-12 blocks" id="commercial" style="display: none;">

						<p class="text-center">Deep Down Cleaning Services Ltd. offers a full range of office cleaning services, including: dusting, vacuuming, sweeping, mopping, upholstery and carpet cleaning, stripping and waxing floors, window and partition cleaning, washing walls and fixtures, and trash removal.</p>

						<p class="text-center">Our professional and uniformed staff are WHMIS trained, bonded, have passed all background checks and have signed confidentiality agreements.</p>

						<p class="text-center">Deep Down Cleaning Services Ltd. offers competitive wages, which minimizes turnover, and as a direct result this is how you will experience consistent and efficient services.</p>

						<p class="text-center">Our dedicated owner-managers follow-up and visit the site regularly to ensure all work is carried out to the high standards our clients expect.</p>

						<!--<p class="text-center bottom-top-margin"><strong>FREE ESTIMATES</strong></p>-->

					</div>



					<!-- Common Block -->
					<div class="col-12 blocks" id="common" style="display: none;">

						<p class="text-center">Deep Down Cleaning Services Ltd. provide a professional common area cleaning service that is customized to fit your specific needs. Whether you require us daily, weekly or to cover vacations we will be there when you need us. We can accommodate whatever fits your budget and your needs.</p>

						<p class="text-center">Deep Down Cleaning provides well-trained staff, supervisors and state-of-the-art equipment for all our contracts.</p>

						<p class="text-center">Open communication is well established between the building manager and our staff, and we provide a logbook on site to log any requests or questions that may arise.</p>

						<p class="text-center">Deep Down Cleaning managers maintain hands-on involvement with their accounts and visit the sites regularly to ensure quality assurance is being met at all times.</p>

						<!--<p class="text-center bottom-top-margin"><strong>Call for your free estimate</strong></p>-->

					</div>

					<!-- 24 Block -->
					<div class="col-12 blocks" id="24" style="display: none;">

						<p class="text-center">At Deep Down Cleaning Services Ltd., we have staff available for unforeseen events that require immediate attention. This is an 'Added Value' service available to existing clients who are currently under contract. </p>

						<p class="text-center">Deep Down Cleaning is extremely dependable: we provide fast reliable service 24/7, 365 days a year.</p>
						<p class="text-center">When an emergency strikes, our professional staff will be there for you.</p>

					</div>

					<!-- Post Block -->
					<div class="col-12 blocks" id="post" style="display: none;">

						<p class="text-center">Now that you have invested so much time, money and effort into remodeling or building your new space, we are certain you will want its final presentation to be clean and attractive. Don’t move in until Deep Down Cleaning Services Ltd. provides the post-construction clean up. The interior and exterior of your property will look clean and smell fresh.</p>

						<p class="text-center">Our trained employees will clean your site to the highest of standards. We do general cleaning and dusting, clean windows and window frames, strip and wax floors, as well as carpet clean and deodorize. Don’t stress, Deep Down Cleaning will take care of your mess! </p>

						<!--<p class="text-center bottom-top-margin"><strong>Call for your free estimate</strong></p>-->

					</div>
         

					<!-- Pressure Block -->
					<div class="col-12 blocks" id="pressure" style="display: none;">

						<p class="text-center">Whether we are cleaning graffiti, apartment decks, canopies or sidewalks, every surface is different. Deep Down Cleaning Services Ltd. uses specialized cleaning applications for each individual surface of your commercial property.</p>

						<p class="text-center">Pressure washing will get rid of ugly mold, mildew, fungus, algae and grime, etc. Power washing extends the life of expensive paint, wood surfaces, concrete, stucco for years to come.</p>

						<p class="text-center">Your property is a major investment in your business and should always look its best. Maintaining your property's condition and appearance will impress your customers, and maintain the value of your investment.</p>

						<p class="text-center"> Learn more about our pressure washing services on <a id="only-mobile" href="http://thebigwash.ca/" target="_blank">thebigwash.ca</a><a href="http://thebigwash.ca/" target="_blank"><img class="img-fluid aaaaaa" src="images/thebigwashlogo2.png"></a></p>

					<div class="col-12 text-center">
						<a href="http://thebigwash.ca/" target="_blank"><img class="img-fluid aaa" src="images/thebigwashlogo2.png"></a>
					</div>

					</div>



					
           

					<!-- Floods Block -->
					<div class="col-12 blocks" id="floods" style="display: none;">

						<p class="text-center">Faulty plumbing, broken appliances or bad storms can push water inside your property and cause flooding. Water can cause costly damage to your carpets and underpadding, furnishings, electronics and appliances.</p>

						<p class="text-center">At Deep Down Cleaning Services Ltd. we feel that immediate action is important, not only so you can get your life back in order, but also to minimize the damage caused and the cost of repairs.</p>

						<p class="text-center margin-top">We are IICRC (Clean Trust) certified and are only a phone call away.</p>

					</div>

					<div class="col-12 text-center margin-top-larger">
						<button id="second-btn" onclick="scrollIt('contact-section')" class="btn btn-default text-center ">CONTACT US</button>
					</div>

			</div> <!-- End of Services Container -->
				<div class="bubble4"><img src="images/water-bubble.png"></div>

				<div class="bubble5"><img src="images/water-bubble.png"></div>

				<div class="bubble7"><img src="images/water-bubble.png"></div>

				<div class="bubble8"><img src="images/water-bubble.png"></div>
			
		</div> <!-- End of Services Container Fluid -->



		<div class="container testimonials-container hideme" id="testimonials-section">
			<div class="row" data-aos="fade-left">

				<div class="col-12" style="display: inline-grid;">

				
				<div class="col-12 text-right align-self-center" style="position: absolute; align-self: center;"><img class="arrow22 img-fluid" src="images/next.png" style="margin-right: -50px;"></div>
				<div class="col-6 text-left align-self-center" style="position: absolute; align-self: center;"><img class="arrow23 img-fluid" style="margin-left: -50px;" src="images/next-left.png"></div>


					<h2 class="text-center" style="color: rgb(237,41,123); font-weight: bold !important; color: #ea297a;">TESTIMONIALS</h2>
					<h2 class="text-center bottom-top-margin" style="color:rgb(17,28,107);">Customer Satisfaction is our number one priority!</h2>

					<div id="first-testimonials" style="display: block;">
						<p class="text-left margin-top-larger">"We have been utilizing a number of services provided by Deep Down Cleaning for over 8 years. The owners of Deep Down Cleaning, Richard Abbass and Neil Bates, are responsive to all our needs. Their work has been dependable and thorough, and our tenants have been very complimentary about the services provided by Deep Down Cleaning.
						We will continue to use the services provided by Deep Down Cleaning and would recommend their service to other landlords and businesses."</p>
						<p class="text-right bottom-top-margin"><i>Dwayne - Property Manager</i></p>
						<p class="text-left">"We have always been completely satisfied with Deep Down Cleaning's customer service and professional approach. They have continually been available at any time, day or night, and in our line of work this is very important. We will continue to use Deep Down Cleaning and would highly recommend their services."</p>
						<p class="text-right bottom-top-margin"><i>Christine - Property Manager</i></p>
					</div>

					<div id="second-testimonials" style="display: none;">
						<p class="text-left margin-top-larger">"Using the best and safest chemicals, and the very best of equipment is a priority to us. We believe that Deep Down Cleaning strives to do the best job possible and we recommend their services to any customer requiring the best carpet cleaners in Metro."</p>
						<p class="text-right bottom-top-margin"><i>Mike - Supplier</i></p>
						<p class="text-left margin-top-larger">"Thank you for your excellent work in the interim, and we greatly appreciate your flexibility and fast action with such short notice."</p>
						<p class="text-right bottom-top-margin"><i>Mike - Property Manager</i></p>
					</div>
					<div class="w-100 text-center align-content-center margin-top-larger">
						<div class="circle text-center" id="first-circle" style="display: inline-flex; background-color:rgb(17,28,107);"></div>
						<div class="circle text-center" id="second-circle" style="display: inline-flex; margin-left:10px;"></div>
					</div>
					<div class="w-100 text-center align-content-center" style="margin-top: -5px;">
						<div class="text-center" style="display: inline-flex; width: 20px; color:rgb(17,28,107);">1</div>
						<div class="text-center" style="display: inline-flex; margin-left: 10px; width: 9px; color:rgb(17,28,107);">2</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container news-container margin-top hideme" id="news-section">
			<div class="row" data-aos="fade-right">
				<div class="col-12">
					<h2 class="text-center margin-top" style="color: rgb(237,41,123); font-weight: bold !important; color: #ea297a;">NEWS</h2>
					<h2 class="text-center bottom-top-margin" style="color:rgb(17,28,107);">Residential Carpet Cleaning</h2>
					<p class="text-center line">"Deep Down Cleaning Services really Sucks when it Counts!"</p>

					<p class="text-center bottom-top-margin line">Powerful Extraction & Quicker Drying - truck-mounted! All our carpet cleaning prices include free deodorization.</p>

					<p class="text-center line">We clean carpet. We clean upholstery. We even clean car upholstery!</p>

					<p class="text-center line">With free deodorization included every time!</p>
				</div>
			</div>
		</div>

		<div class="bubble6"><img src="images/water-bubble.png"></div>

		<div class="container specials-container bottom-top-margin hideme" id="specials-section">
			<div class="row" data-aos="fade-left">
				<div class="col-12">
					<h2 class="text-center margin-top" style="color: rgb(237,41,123); font-weight: bold !important; color: #ea297a;">SPECIALS</h2>
					<h2 class="text-center bottom-top-margin" style="color:rgb(17,28,107);">Residential Carpet Cleaning</h2>
					<p class="text-center line">"Deep Down Cleaning Services really Sucks when it Counts!"</p>

					<p class="text-center bottom-top-margin line">Powerful Extraction & Quicker Drying - truck-mounted! All our carpet cleaning prices include free deodorization.</p>

					<p class="text-center line">We clean carpet. We clean upholstery. We even clean car upholstery!</p>

					<p class="text-center line">With free deodorization included every time!</p>
				</div>
			</div>
		</div>

	</div>

	<?php require_once "footer.php"; ?>

<script>
	$(".sections").click(function() {
		$(".sections").removeClass('selected');
	  	$(this).addClass('selected');
	  	var value = $(this).text();
	  	$("#changing-service-name").html(value);
	  	$(".blocks").css('display','none');
	  	$("#changing-service-name").css('color','rgb(17,28,107)');
	  	$(".telephone").attr("href", "tel:9024255619");
	  	$("#first-btn").css('background-color','rgb(237,41,123)');
	  	if(value=="Residential/Commercial Carpet & Upholstery Cleaning"){
	  		$("#changing-image").attr('src','images/residential.jpg');
	  		$("#upholstery").css('display','block');
	  	}
	  	else if(value=="Parkade Machine Power Sweeping & Scrubbing"){
	  		$("#changing-image").attr('src','images/Parkade-machine-sweeping-and-scrubbing.jpg');
	  		$("#parkade").css('display','block');
	  	}
	  	else if(value=="Outdoor Machine Power Sweeping"){
	  		$("#changing-image").attr('src','images/Outdoor-machine-power-sweeping.jpg');
	  		$("#outdoor").css('display','block');
	  	}
	  	else if(value=="Parking Lot Cleaning & Management"){
	  		$("#changing-image").attr('src','images/Parking-lot-cleaning-and-management.jpg');
	  		$("#parking").css('display','block');
	  	}
	  	else if(value=="Stripping & Waxing Floors"){
	  		$("#changing-image").attr('src','images/stripping.jpg');
	  		$("#stripping").css('display','block');
	  	}
	  	else if(value=="Commercial Office Cleaning"){
	  		$("#changing-image").attr('src','images/Commercial-office-cleaning.jpg');
	  		$("#commercial").css('display','block');
	  	}
	  	else if(value=="Common Area Building Cleaning"){
	  		$("#changing-image").attr('src','images/Common-area-building-cleaning.jpg');
	  		$("#common").css('display','block');
	  	}
	  	else if(value=="24 Hour Emergency Service"){
	  		$("#changing-image").attr('src','images/EMERGENCY-CALL.jpg');
	  		$("#24").css('display','block');
	  	}
	  	else if(value=="Post-Construction Clean Up"){
	  		$("#changing-image").attr('src','images/Post-construction-cleanup.jpg');
	  		$("#post").css('display','block');
	  	}
	  	else if(value=="Pressure Washing"){
	  		$("#changing-image").attr('src','images/pressure.jpg');
	  		$("#pressure").css('display','block');
	  	}
	  	else{
	  		$("#changing-image").attr('src','images/Floods-water-restoration.jpg');
	  		$("#floods").css('display','block');
	  		$(".telephone").attr("href", "tel:9028301113");
	  	}
	});

</script>

<script>
	$(".circle").click(function() {
		$(".circle").css('background-color','white');
		$(this).css('background-color','rgb(17,28,107)');
	});

	$("#first-circle").click(function(){
		$("#first-testimonials").css('display','block');
		$("#second-testimonials").css('display','none');
	});

	$("#second-circle").click(function(){
		$("#second-testimonials").css('display','block');
		$("#first-testimonials").css('display','none');
	});

	$(".arrow22").click(function(){
		$("#second-testimonials").css('display','block');
		$("#first-testimonials").css('display','none');
		$("#first-circle").css('background-color','white');
		$("#second-circle").css('background-color','rgb(17,28,107)');

	});

	$(".arrow23").click(function(){
		$("#first-testimonials").css('display','block');
		$("#second-testimonials").css('display','none');
		$("#second-circle").css('background-color','white');
		$("#first-circle").css('background-color','rgb(17,28,107)');
	});


</script>
<script type="text/javascript" src="assets/js/scroll-to-section.js">
</script>
<script>
	AOS.init();
</script>
</body>
</html>