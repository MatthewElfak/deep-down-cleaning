<!DOCTYPE html>
<html lang="EN">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="library/jquery/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="library/bootstrap/css/bootstrap.min.css">
	<script src="library/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/header.css">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>

	<div class="container-fluid fixed-top header-desktop" style="z-index: 5000;">

		<!--<div class="row h-100">
			<div class="col-6 text-left align-self-center">
				<div style=" background-color: white; border-radius: 50%; overflow: hidden; width: 170px; padding: 20px; margin-top: -20px;">
					<img class="img-fluid" src="images/logo.jpg" alt="">
				</div>
			</div>
			<div class="col-6 text-right align-self-center">
				<p><a style="text-decoration: none; color: #fff;" href="tel:9024255619">(902) 425-5619</a> <span class="icon"><a style="text-decoration: none; color: #fff;" href="https://www.facebook.com/pages/Deep-Down-Cleaning-Services-Ltd/1047180455370541" target="_blank"><i class="fab fa-facebook-f"></i></a></span><span class="icon"><i class="far fa-envelope"></i></span></p>
			</div>
		</div>-->

		<div class="row h-100">
			<div class="col-2 col-md-2">
				<a href="index.php"><img class="img-fluid" src="images/logoo.png" alt=""></a>
			</div>
			<div class="col-7 col-sm-7 col-md-7 header-nav-desktop align-middle align-self-center" style="padding-left: 0px; padding-right: 0px;">
				<ul class="list-inline" style="cursor: pointer; margin-top: 16px;">
					<li id="redIt" onclick="scrollIt('who-section')" class="list-inline-item">ABOUT US</li>
					<a onclick="scrollIt('services-section')"><li class="list-inline-item">SERVICES</li></a>
					<a onclick="scrollIt('testimonials-section')"><li class="list-inline-item">TESTIMONIALS</li></a>
					<a class="text-center" onclick="scrollIt('news-section')"><li class="list-inline-item">NEWS</li></a>
					<a onclick="scrollIt('specials-section')"><li class="list-inline-item">SPECIALS</li></a>
					<a onclick="scrollIt('career-section')"><li class="list-inline-item">CAREERS</li></a>
					<a onclick="scrollIt('contact-section')"><li class="list-inline-item">CONTACT US</li></a>
				</ul>
			</div>
			<div class="col-3 col-sm-3 col-md-3 align-self-center header-nav-desktop" style="padding-left: 0px; padding-right: 0px;">
				<ul class="list-inline text-center" style="margin-top: 30px; font-size: 1rem;">
					<li class="list-inline-item">
						<a id="broj" style="text-decoration: none; color: #fff;" href="tel:9024255619">(902) 425-5619</a>
					</li>
					<li class="list-inline-item" id="pomoc" >
						<p><span class="icon icon2"><a style="text-decoration: none;" href="https://www.facebook.com/pages/Deep-Down-Cleaning-Services-Ltd/1047180455370541" target="_blank"><i class="fab fa-facebook-f" style="border-radius: 63%;
    border: 2px solid white;
    /* padding: 10px; */
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 9px;
    padding-bottom: 10px;"></i></a></span><span class="icon icon1"><i onclick="scrollIt('contact-section')" class="far fa-envelope" style="border-radius: 60%;
    border: 2px solid white;
    padding: 10px 11px;"></i></span></p>
					</li>
				</ul>
			</div>
		</div>

	</div>












	<div class="container-fluid fixed-top header-mobile" style="padding-bottom: 0px; z-index: 4999 !important;">
		<div class="row" style="margin-bottom: 0px;">
			<div class="col-6 text-center">
				<a href="index.php"><img style="height: 70px; margin-top: -3px;" class="img-fluid" src="images/logoo.png" alt=""></a>	
			</div>

			<div class="col-6 text-center" style="padding-top: 5px;">
				<p style="margin-bottom: 0px;"><a style="text-decoration: none; color: #fff;" href="tel:9024255619">(902) 425-5619</p>
				<p style="margin-left: -12px;" class="text-center"><span class="icon1"><a style="text-decoration: none;" href="https://www.facebook.com/pages/Deep-Down-Cleaning-Services-Ltd/1047180455370541" target="_blank"><i class="fab fa-facebook-f" style="border-radius: 63%; border: 1px solid white; padding-left: 13px;
			    padding-right: 13px; padding-top: 8px; padding-bottom: 7px;"></i></a></span><span class="icon1"><i class="far fa-envelope" style="border-radius: 60%; border: 1px solid white; padding: 7px 7px;"></i></span></p>
			</div>
		</div>

		<div class="row">
			<div class="col-12 text-center">
				<h6 style="cursor: pointer;" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">MENU</h6>
				<div class="text-center"><img src="images/down.png" style="width: 27px;"></div>
				<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				    <div class="navbar-nav">
				      <a class="nav-item nav-link active" onclick="scrollIt('who-section')" href="#">About Us <span class="sr-only">(current)</span></a>
				      <a onclick="scrollIt('services-section')" class="nav-item nav-link" href="#">Services</a>
				      <a onclick="scrollIt('testimonials-section')" class="nav-item nav-link" href="#">Testimonials</a>
				      <a onclick="scrollIt('news-section')" class="nav-item nav-link" href="#">News</a>
				      <a onclick="scrollIt('specials-section')" class="nav-item nav-link" href="#">Specials</a>
				      <a onclick="scrollIt('career-section')" class="nav-item nav-link" href="#">Careers</a>
				      <a onclick="scrollIt('contact-section')" class="nav-item nav-link" href="#">Contact Us</a>
				    </div>
				</div>
			</div>
		</div>
	</div>


	
</body>
</html>